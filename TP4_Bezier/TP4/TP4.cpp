// SegmentsCercles.cpp�: d�finit le point d'entr�e pour l'application console.
//
#include "stdafx.h"



#include <vector>
#include <iostream>

using namespace std;

struct Vect2D
{
	float x;
	float y;
};

int vx1 = 0, vx2 = 400, vy1 = 0, vy2 = 400;
std::vector<Vect2D> pointsControles;


float precision = 0.01f;


int fact(int nb)
{
	if (nb == 0 || nb == 1)
		return 1;

	int sum = 1;
	for (int i = 1; i <= nb; i++)
		sum *= i;

	return sum;
}

std::vector<Vect2D> calculPoint(std::vector<Vect2D> pointsControles, int nbPoints, float i)
{
	std::vector<Vect2D> resultats;
	resultats.resize(nbPoints - 1);
	for (int n = 0; n < nbPoints-1; n++)
	{
		Vect2D point1 = pointsControles[n];
		Vect2D point2 = pointsControles[n + 1];

		Vect2D vect;
		vect.x = point2.x - point1.x;
		vect.y = point2.y - point1.y;

		Vect2D pointRes;
		pointRes.x = point1.x + i * vect.x;
		pointRes.y = point1.y + i * vect.y;

		resultats[n] = (pointRes);
		
		glColor3f(0.0, 1.0, 0.0);
		glFlush();

	}

	if (resultats.size() > 1)
		resultats = calculPoint(resultats, resultats.size(), i);

	return resultats;
}

// Autre algorithme similaire � ce que l'on � fait en cours en tracant les droites intermediaires jusqu'� en avoir plus qu'une
void bezier(std::vector<Vect2D> pointsControles)
{

	if (pointsControles.size() == 0)
		return;

	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_LINE_STRIP);

	for (float i = 0.0f; i < 1; i = i + precision)
	{

		 
		std::vector<Vect2D> point = calculPoint(pointsControles, pointsControles.size(), i);
			glVertex2f(point[0].x, point[0].y);
		
	}
	glEnd();


	glBegin(GL_LINE_STRIP);
	glColor3f(1.0, 1.0, 1.0);
	for (unsigned int i = 0; i < pointsControles.size(); i++)
		glVertex2f(pointsControles[i].x, pointsControles[i].y);

	glEnd();
}

void bezier2(std::vector<Vect2D> pointsControles)
{
	if (pointsControles.size() == 0)
		return;

	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_LINE_STRIP);

	for (float t = 0.0f; t < 1; t = t + 0.01f)
	{
		Vect2D pointI;
		pointI.x = 0.0f;
		pointI.y = 0.0f;
		//cout << pointI.x << ", " << pointI.y << endl;

		int n = pointsControles.size() - 1;
		for (int i = 0; i <= n; i++)
		{
			float Bn = (float)((float)fact(n) * pow(1.0f - t, n - i) * (float)pow(t, i)) / (float)(fact(i) * fact(n - i));

			pointI.x += (pointsControles[i].x * Bn);
			pointI.y += (pointsControles[i].y * Bn);		
		}
		glVertex2f(pointI.x, pointI.y);
	}

	glEnd();
	

	glBegin(GL_LINE_STRIP);
	glColor3f(1.0, 1.0, 1.0);
	for (unsigned int i = 0; i < pointsControles.size(); i++)
		glVertex2f(pointsControles[i].x, pointsControles[i].y);

	glEnd();
	
}


void affichage(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	
	//bezier(pointsControles);
	bezier2(pointsControles);

	glFlush();
}

void MouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		Vect2D point;
		point.x = x;
		point.y =  vy2 - y;

		pointsControles.push_back(point);
		glutPostRedisplay();
	}	
}

int _tmain(int argc, char* argv[])
{
	// init window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowPosition(vx1, vy1);
	glutInitWindowSize(vx2, vy2);
	glutCreateWindow("ogl1");

	// init OpenGL settings
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glPointSize(2.0);
	//glLineWidth(5);

	glOrtho(vx1, vx2, vy1, vy2, -1.0, 1.0); //coordonn�es des limites de la vue


	// GLUT callbacks
	glutDisplayFunc(affichage);
	glutMouseFunc(MouseButton);



	// GLUT main loop
	glutMainLoop();
	return 0;
}

