#include "Circle.h"


Circle::Circle(int x1, int y1, int x2, int y2, float r, float g, float b) : Shape(r,g,b) {
	_centre.x = x1;
	_centre.y = y1;
	_radius = sqrt(pow((float)x1 - y1, 2) + pow((float)x2 - y2, 2));
	//_end.x = x2;
	//_end.y = y2;
}

Circle::~Circle(){}

void Circle::draw() {
	int x1 = _centre.x;
	int y1 = _centre.y;
	//int x2 = _end.x;
	//int y2 = _end.y;

	glColor3f(_color.r, _color.g, _color.b);

	//glPointSize(1.0);
	//glOrtho(vx1 - vx2 / 2, vx2 / 2, vy1 - vy2 / 2, vy2 / 2, -1.0, 1.0); //coordonnées des limites de la vue

	int radius = floor(_radius); 

	int x = radius;
	int y = 0;
	int radiusError = 1 - x;

	while (x >= y) {
		glVertex2i(x + x1, y + y1);
		glVertex2i(y + x1, x + y1);
		glVertex2i(-x + x1, y + y1);
		glVertex2i(-y + x1, x + y1);
		glVertex2i(-x + x1, -y + y1);
		glVertex2i(-y + x1, -x + y1);
		glVertex2i(x + x1, -y + y1);
		glVertex2i(y + x1, -x + y1);
		y++;
		if (radiusError<0){
			radiusError += 2 * y + 1;
		}
		else {
			x--;
			radiusError += 2 * (y - x + 1);
		}
	}
}

void Circle::setEnd(int x, int y) {
	_radius = sqrt(pow((float)_centre.x - _centre.y, 2) + pow((float)x - y, 2));
}