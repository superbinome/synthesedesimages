#pragma once
#include "Shape.h"
class Line : public Shape {
private:
	Point _start;
	Point _end;
public:
	Line(int x1, int y1, int x2, int y2, float r, float g, float b);
	Line(Point start, Point end, float r, float g, float b);
	~Line();

	void draw();

	Point getStart();
	Point getEnd();

	void setStart(int x, int y);
	void setEnd(int x, int y);
};

