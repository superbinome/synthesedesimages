#pragma once
#include "glut.h"

typedef struct Point {
	int x;
	int y;
} Point;

typedef struct Color {
	float r;
	float g;
	float b;
} Color;

class Shape {
protected:
	Color _color;
public:
	Shape(float r, float g, float b);
	virtual ~Shape();

	virtual void draw() =0;

	Color getColor();

	virtual void setEnd(int x, int y) = 0;
	void setColor(Color color);
};

