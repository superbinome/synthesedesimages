#include "Shape.h"


Shape::Shape(float r, float g, float b) {
	_color.r = r;
	_color.g = g;
	_color.b = b;
}

Shape::~Shape() {}

Color Shape::getColor() {
	return this->_color;
}

void Shape::setColor(Color color) {
	this->_color = color;
}
