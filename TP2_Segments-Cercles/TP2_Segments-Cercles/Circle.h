#pragma once
#include "Shape.h"
#include <cmath>
#include <math.h>

class Circle : public Shape {
private:
	Point _centre;
	float _radius;
public:
	Circle(int x1, int y1, int x2, int y2, float r, float g, float b);
	~Circle();

	void draw();
	
	void setEnd(int x, int y);
};

