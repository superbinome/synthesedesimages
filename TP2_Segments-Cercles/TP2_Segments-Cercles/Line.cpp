#include "Line.h"


Line::Line(int x1, int y1, int x2, int y2, float r, float g, float b) : Shape( r, g, b) {
	_start.x = x1;
	_start.y = y1;
	_end.x = x2;
	_end.y = y2;
}

Line::~Line() {}

void Line::draw() {
	int x1 = _start.x;
	int y1 = _start.y;
	int x2 = _end.x;
	int y2 = _end.y;

	glColor3f(_color.r, _color.g, _color.b);

	int dx = x2 - x1;
	int dy = y2 - y1;

	//glPointSize(1.0);//taille d'un point: 1 pixel
	//glOrtho(vx1 - vx2 / 2, vx2 / 2, vy1 - vy2 / 2, vy2 / 2, -1.0, 1.0); //coordonn�es des limites de la vue

	if (dx != 0) {
		if (dx > 0) {
			if (dy != 0) {
				if (dy > 0) { // vecteur oblique (ni vertical ni horizontal) dans le 1er quadran
					if (dx >= dy) { // vecteur dans le 1er octant
						int e = dx;
						dx = e * 2;
						dy = dy * 2; // e est positif
						while (x1 < x2) {
							glVertex2i(x1, y1);
							x1++;
							e -= dy;
							if (e <= 0) {
								y1++;  // d�placement diagonal
								e += dx;
							}
						}
					}
					else {
						// vecteur oblique proche de la verticale, dans le 2nd octant
						int e = dy;
						dy = e * 2;
						dx = dx * 2;  // e est positif
						while (y1 < y2) {  // d�placements verticaux
							glVertex2i(x1, y1);
							y1++;
							e -= dx;
							if (e <= 0) {
								x1++;  // d�placement diagonal
								e += dy;
							}
						}
					}
				}
				else { // dy < 0 (et dx > 0)
					// vecteur oblique dans le 4e cadran

					if (dx >= -dy) {
						// vecteur diagonal ou oblique proche de l�horizontale, dans le 8e octant
						int e = dx;
						dx *= 2;
						dy *= 2;  // e est positif
						while (x1 < x2) {  // d�placements horizontaux
							glVertex2i(x1, y1);
							x1++;
							e += dy;
							if (e <= 0) {
								y1 -= 1;  // d�placement diagonal
								e += dx;
							}
						}
					}
					else {  // vecteur oblique proche de la verticale, dans le 7e octant
						int e = dy;
						dy *= 2;
						dx *= 2;  // e est n�gatif
						while (y2 < y1) {  // d�placements verticaux
							glVertex2i(x1, y1);
							y1--;
							e += dx;
							if (e > 0) {
								x1++;  // d�placement diagonal
								e += dy;
							}
						}
					}
				}
			}
			else {  // dy = 0 (et dx > 0)
				// vecteur horizontal vers la droite
				while (x1 < x2) {
					glVertex2i(x1, y1);
					x1++;
				}
			}
		}
		else {  // dx < 0
			dy = y2 - y1;
			if (dy != 0) {
				if (dy > 0) {
					// vecteur oblique dans le 2nd quadran
					if (-dx >= dy) {
						// vecteur diagonal ou oblique proche de l�horizontale, dans le 4e octant
						int e = dx;
						dx *= 2;
						dy *= 2;  // e est n�gatif
						while (x1 > x2) {  // d�placements horizontaux
							//glBegin(GL_POINTS);
							glVertex2i(x1, y1);
							//glEnd();//fin du mode point
							x1--;
							e += dy;
							if (e >= 0) {
								y1++;  // d�placement diagonal
								e += dx;
							}
						}
					}
					else {
						// vecteur oblique proche de la verticale, dans le 3e octant
						int e = dy;
						dy *= 2;
						dx *= 2;  // e est positif
						while (y1 < y2) {  // d�placements verticaux
							glVertex2i(x1, y1);
							y1++;
							e += dx;
							if (e <= 0) {
								x1--;  // d�placement diagonal
								e += dy;
							}
						}
					}
				}
				else {  // dy < 0 (et dx < 0)
					// vecteur oblique dans le 3e cadran
					if (dx <= dy) {
						// vecteur diagonal ou oblique proche de l�horizontale, dans le 5e octant
						int e = dx;
						dx *= 2;
						dy *= 2;  // e est n�gatif
						while (x1 > x2) {  // d�placements horizontaux
							glVertex2i(x1, y1);
							x1--;
							e -= dy;
							if (e >= 0) {
								y1--;  // d�placement diagonal
								e += dx;
							}
						}
					}
					else {  // vecteur oblique proche de la verticale, dans le 6e octant
						int e = dy;
						dy *= 2;
						dx *= 2;  // e est n�gatif
						while (y1 > y2) {  // d�placements verticaux
							glVertex2i(x1, y1);
							y1--;
							e -= dx;
							if (e >= 0) {
								x1--;  // d�placement diagonal
								e += dy;
							}
						}
					}
				}
			}
			else {  // dy = 0 (et dx < 0)

				// vecteur horizontal vers la gauche
				while (x1 > x2) {
					glVertex2i(x1, y1);
					x1--;
				}
			}
		}
	}
	else {  // dx = 0
		dy = y2 - y1;
		if (dy != 0) {
			if (dy > 0) {

				// vecteur vertical croissant
				while (y1 < y2) {
					glVertex2i(x1, y1);
					y1++;
				}
			}
			else {  // dy < 0 (et dx = 0)

				// vecteur vertical d�croissant
				while (y1 > y2) {
					glVertex2i(x1, y1);
					y1--;
				}
			}
		}
	}
	glVertex2i(x2, y2);// n�est pas trac�.
}

Point Line::getStart() {
	return this->_start;
}

Point Line::getEnd() {
	return this->_end;
}

void Line::setStart(int x, int y) {
	this->_start.x = x;
	this->_start.y = y;
}

void Line::setEnd(int x, int y) {
	this->_end.x = x;
	this->_end.y = y;
}