/**************************************************************************/
/* TP2 - Trac�s de lignes et de cercles
/**************************************************************************/
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "glut.h"
#include "Line.h"
#include "Circle.h"


#define MAXSHAPES 256

void createMenu();
void display();
void drawCircle(int x0, int y0, int x1, int y1);
void drawLine(int x1, int y1, int x2, int y2);
void drawShapes();
void menuFunction(int value);
void mouseDrag(int x, int y);
void mouseInput(int button, int state, int x, int y);
void reshape(int width, int height);


typedef enum mouseState {
	waiting,
	clickedOnce,
};

typedef enum mode {
	line,
	circle,
	polygon,
};

std::vector<Shape*>* shapes;

//shape shapes[MAXSHAPES]; // tableau de stockage des diff�rentes formes (256 au max)
//int numShapes; // Nombre de formes cr��es

int mouseState = waiting; // �tat de la souris - en attente de clic
int mode = line; //mode du trac�

int vx1=0,vx2=400,vy1=0,vy2=400; // taille de la fen�tre

float RVB[3] = { 1.0, 1.0, 1.0 }; // stockage de la couleur de dessin
Color c;

// fonctions du menu 
void menuFunction(int value) {
	switch (value) {
	case 0:
		c.r = 1.0;
		c.g = 0.0;
		c.b = 0.0;
		break;
	case 1:
		c.r = 0.0;
		c.g = 1.0;
		c.b = 0.0;
		break;
	case 2:
		c.r = 0.0;
		c.g = 0.0;
		c.b = 1.0;
		break;
	case 3:
		c.r = 1.0;
		c.g = 1.0;
		c.b = 0.0;
		break;
	case 4:
		c.r = 1.0;
		c.g = 0.0;
		c.b = 1.0;
		break;
	case 5:
		c.r = 0.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	case 6:
		c.r = 1.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	case 8:
		mode = line; // on passe en mode ligne
		break;
	case 9:
		mode = circle; // on passe en mode cercle
		break;
	default: // Si on appuie sur la ligne s�paratrice : blanc
		c.r = 1.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	}
	shapes->back()->setColor(c);
}

// initialisation du menu
void createMenu() {
	int menu = glutCreateMenu(menuFunction);

	glutAddMenuEntry("Red", 0);
	glutAddMenuEntry("Green", 1);
	glutAddMenuEntry("Blue", 2);
	glutAddMenuEntry("Yellow", 3);
	glutAddMenuEntry("Magenta", 4);
	glutAddMenuEntry("Cyan", 5);
	glutAddMenuEntry("White",6);
	glutAddMenuEntry("--------------", 7);
	glutAddMenuEntry("Line Mode", 8);
	glutAddMenuEntry("Circle Mode", 9);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

//cette fonction est la fonction de gestion des mouvement de la souris
void mouseDrag(int x,int y) {
	if (mouseState == clickedOnce) {
		shapes->back()->setEnd(x, vy2 - y);
	}
	std::cout << x << "," << y << std::endl;
	glutPostRedisplay();
}

//cette fonction g�re les click souris
void mouseInput(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		switch (mouseState) {
		case waiting: //d�finition du premier point
			//si on a appuy� sur le bouton gauche
			if (mode == line)
				shapes->push_back(new Line(x, vy2 - y, x, vy2 - y, c.r, c.g, c.b));
			else
				shapes->push_back(new Circle(x, vy2 - y, x, vy2 - y, c.r, c.g, c.b));
			mouseState++;
			break;
		case clickedOnce: //d�finition du second point
			shapes->back()->setEnd(x, vy2 - y);
			mouseState = waiting;
			break;
		default:
			break;
		}
		std::cout <<"clic:" << x << "," << y << std::endl;
	}
	glutPostRedisplay();
}

//Cette fonction est la fonction de gestion du redimensionnement
void reshape(int width,int height) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, width, 0, height);
	
	//mise � jour des valeurs de dimensions
	vx1=0;
	vx2=width;
	vy1=0;
	vy2=height;

	glMatrixMode(GL_MODELVIEW);

	//d�finition de la nouvelle vue
	//glViewport(0,0,largeur,hauteur);
}

void drawShapes() {
	glColor3fv(RVB);
	glBegin(GL_POINTS);

	for (auto it = shapes->begin(); it != shapes->end(); it++) {
		(*it)->draw();
	}
	glEnd();
}

//cette fonction est la fonction d'affichage principale
void display() {
	//glViewport(0,20,vx2,vy2);//d�finition de la vue
	glClear(GL_COLOR_BUFFER_BIT); //on efface
	//drawAxis();//on affiche les axes
	drawShapes();
	glutSwapBuffers();
}

int main(int argc,char **argv) {
	glutInit(&argc,argv); //initialisation de glut

	// init window
	glutInitDisplayMode(GLUT_RGB|GLUT_DOUBLE); //initialisation de l'affichage
	glutInitWindowPosition(vx1,vy1); //initialisation de la position de la fen�tre
	glutInitWindowSize(vx2,vy2); //initialisation de la taille de la fen�tre
	glutCreateWindow("TP2 - Segments & Cercles"); //cr�ation de la fen�tre

	// GLUT callbacks
	glutDisplayFunc(display); //attachement de la fonction d'affichage
	glutReshapeFunc(reshape); //attachement de la fonction de redimensionnement
	glutMouseFunc(mouseInput); //attachement de la fonction de gestion de la souris
	glutPassiveMotionFunc(mouseDrag); //attachement de la fonction de gestion des mouvement de la souris

	// init OpenGL settings
	glClearColor(0.0, 0.0, 0.0, 0.0); //couleur d'effacement: noir
	glMatrixMode(GL_PROJECTION);
	glOrtho(-1, 1.0, -1, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glPointSize(1.0); //taille d'un point: 1 pixels
	glColor3f(0.0, 1.0, 0.0); //couleur verte

	shapes = new std::vector<Shape*>();
	c.r = 0.0; c.g = 1.0; c.b = 0.0;
	
	createMenu();

	//lancement de la boucle principale
	glutMainLoop();

	return 0;
}