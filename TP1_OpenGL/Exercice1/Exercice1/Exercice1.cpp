// TP1_Synth_OpenGL.cpp�: d�finit le point d'entr�e pour l'application console.

#define _CRT_SECURE_NO_DEPRECATE

#include "stdafx.h"
#include <GL/glut.h>


#define MAXC 20
#define MAXV 100
float courbe[MAXC][MAXV];
float valmin[MAXC];
float valmax[MAXC];
int nc = 0;


int vx1 = 0, vx2 = 400, vy1 = 0, vy2 = 400;

// Resize
void retailler(int w, int h)
{
	vy2 = h;
	vx2 = w;
}

void axes(float y, float xa, float xb, float x, float ya, float yb)
{
	glColor3f(1.0, 0, 0);
	glBegin(GL_LINE_STRIP);

	glVertex2f(xa, y);
	glVertex2f(xb, y);

	glEnd();

	glBegin(GL_LINE_STRIP);

	glVertex2f(x, ya);
	glVertex2f(x, yb);

	glEnd();
}


void histo(int k, float a, float b, float c, float d)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// The viewport where we draw the lines
	glViewport((vx2 - vx1)*a, (vy2 - vy1)*b, (vx2 - vx1)*(c - a), (vy2 - vy1)*(d - b));

	glOrtho(0.0, courbe[k][0] - 1, valmin[k], valmax[k], -1.0, 1.0);

	glColor3f(1.0, 1.0, 1.0);

	for (int i = 0; i < courbe[k][0]; i++)
	{
		glBegin(GL_QUADS);

		glVertex2f(i, 0);
		glVertex2f(i, courbe[k][i + 1]);
		glVertex2f(i + 1, courbe[k][i + 1]);
		glVertex2f(i + 1, 0);


		glEnd();

		//printf("X1 = %f\n", i);
		//printf("Y1 = %f\n", courbe[k][i + 1]);
	}





	axes(valmin[k], 0, courbe[k][0] - 1, 0, valmin[k], valmax[k]);
}


void ligne(int k, float a, float b, float c, float d)
{

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// The viewport where we draw the lines
	glViewport((vx2 - vx1)*a, (vy2 - vy1)*b, (vx2 - vx1)*(c - a), (vy2 - vy1)*(d - b));

	glOrtho(0.0, courbe[k][0] - 1, valmin[k], valmax[k], -1.0, 1.0);


	glBegin(GL_LINE_STRIP);
	glColor3f(0.0, 0.0, 1.0);

	for (int i = 0; i < courbe[k][0]; i++)
	{
		glVertex2f(i, courbe[k][i + 1]);

		//printf("X1 = %f\n", i);
		//printf("Y1 = %f\n", courbe[k][i + 1]);
	}

	glEnd();

	axes(valmin[k], 0, courbe[k][0] - 1, 0, valmin[k], valmax[k]);
}

void affichage(void)
{
	// Clear
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);

	for (int i = 0; i < nc; i++)
	{
		float yMin, yMax;
		yMin = (float)i / (float)nc;
		yMax = yMin + 1.0 / (float)nc;

		//printf("yMin = %f", yMin);
		//printf("yMax = %f", yMax);

		histo(i, 0, yMin, 1, yMax);
		ligne(i, 0, yMin, 1, yMax);

	}
	glFlush();
}



void charger(char *nom)
{
	FILE* fichier = fopen(nom, "r");
	if (!fichier)
	{
		printf("Erreur : le fichier %s ne peut pas etre ouvert.", nom);
	}
	else
	{
		fscanf(fichier, "%d", &nc);
		//printf("NC = %d", nc);
		if (nc > MAXC)
		{
			printf("Erreur : le nombre de courbes du fichier est superieur au nombre de courbes max");
			fclose(fichier);
			return;
		}
		for (int i = 0; i < nc; i++)
		{
			int nbElem;
			fscanf(fichier, "%d", &nbElem);
			courbe[i][0] = nbElem;
			printf("courbe[i][n+1] = %d \n", nbElem);
			if (nc > MAXV)
			{
				printf("Erreur : le nombre de nombres d'une courbe du fichier est superieur au nombre de nombres max");
				fclose(fichier);
				return;
			}
			for (int n = 0; n < courbe[i][0]; n++)
			{
				float value;
				fscanf(fichier, "%f", &value);
				courbe[i][n + 1] = value;

				if (courbe[i][n + 1] < valmin[i] || n == 0)
					valmin[i] = courbe[i][n + 1];
				if (courbe[i][n + 1] > valmax[i] || n == 0)
					valmax[i] = courbe[i][n + 1];
			}
		}
		fclose(fichier);
	}
}



int _tmain(int argc, char* argv[])
{
	charger("test_graph.txt");

	// initialisation de GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB); // Initialisation du mode d'affichage (en couleur)
	glutInitWindowPosition(vx1, vy1); // Position de la fenetre (origine : coin haut gauche de l'�cran
	glutInitWindowSize(vx2, vy2); // Taille de la fenetre
	glutCreateWindow(""); // Nom de la fenetre

	// init OpenGL settings
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glPointSize(2.0);
	glLineWidth(5);

	// GLUT callbacks
	glutDisplayFunc(affichage);
	glutReshapeFunc(retailler);


	// GLUT main loop
	glutMainLoop();
	return 0;
}


