// OpenGL_TP1_Ex2.cpp�: d�finit le point d'entr�e pour l'application console.
//



#include "glut.h"
#include "stdafx.h"
#include "math.h"

float time;


// Draw the road
void route(void) {
	float longeurRoute = 20.0;
	float hauteurRoute = 2.0;
	int decal = 0;
	glPushMatrix();
	glTranslatef(-5, -10, 0);
	for (float n = 0.0; n < hauteurRoute / 0.1; n = n + 1) {
		if (decal)
			decal = 0;
		else
			decal = 1;

		for (float i = 0.0; i < longeurRoute / 0.5; i = i + 2) {
			glPushMatrix();
				glTranslatef((i - 10.0)*0.5, n*0.1, 0.0);
				glBegin(GL_QUADS);

				glColor3f(0.0, 0.0, 1.0);
				if (decal)
					glColor3f(0.0, 1.0, 0.0);
				glVertex2f(0.0, 0.0);
				glVertex2f(0.0, 0.1);
				glVertex2f(0.5, 0.1);
				glVertex2f(0.5, 0.0);

				glEnd();
			glPopMatrix();

			glPushMatrix();
				glTranslatef((i+1.0-10.0)*0.5 , n*0.1 , 0.0);
				glBegin(GL_QUADS);
				glColor3f(0.0, 1.0, 0.0);
				if (decal)
					glColor3f(0.0, 0.0, 1.0);
				glVertex2f(0.0, 0.0);
				glVertex2f(0.0, 0.1);
				glVertex2f(0.5, 0.1);
				glVertex2f(0.5, 0.0);

				glEnd();
			glPopMatrix();
		}
	}
	glPopMatrix();
}


void roue(void) {
	// A little box
	glBegin(GL_QUADS);

	glColor3f(0.0, 1.0, 0.0);
	glVertex2f(0.5, 0.5);
	glVertex2f(0.5, -0.5);
	glVertex2f(-0.5, -0.5);
	glVertex2f(-0.5, 0.5);

	glEnd();
}


void caisse(void) {
	// A box
	glBegin(GL_QUADS);

	glColor3f(1.0, 1.0, 1.0);
	glVertex2f(0.0, 0.0);
	glVertex2f(6.0, 0.0);
	glVertex2f(6.0, 2.0);
	glVertex2f(0.0, 2.0);

	glEnd();
}

void voiture(int t) {
	// Reset the projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-10, 10, -10, 10, 1, -1);

	// Reset the transformation matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Draw the road
	route();

	// Position of the car
	glTranslatef(t/100.0 - 10.0, -7.5, 0);
	
	caisse();	// Draw a box
	
	glPushMatrix();						// Store the actual GL_MODELVIEW matrix on the stack
		glTranslatef(5.0, 0.0, 0.0);	// Move the wheel
		glRotatef(-t / 1.0, 0, 0, 1);	// Rotate the wheel
		roue();							// Draw the wheel
	glPopMatrix();						// Cancel the last transformations with the stack

	glPushMatrix();
		glTranslatef(1.0, 0.0, 0.0);
		glRotatef(-t / 1.0, 0, 0, 1);
		roue();
	glPopMatrix();	
}


void affichage(void)
{
	// Clear the screen
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);

	// Render
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	voiture(time*8);
	glutSwapBuffers();

	time++;
}


void idle()
{
	// Refresh
	glutPostRedisplay();
}


int _tmain(int argc, char* argv[])
{
	int vx1 = 0, vx2 = 400, vy1 = 0, vy2 = 400;


	// init window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowPosition(vx1, vy1);
	glutInitWindowSize(vx2, vy2);
	glutCreateWindow("ogl1");

	// init OpenGL settings
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glPointSize(2.0);

	// GLUT callbacks
	glutDisplayFunc(affichage);
	glutIdleFunc(idle);


	// GLUT main loop
	glutMainLoop();
	return 0;
}

