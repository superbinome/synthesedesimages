/**************************************************************************/
/* TP2 - Trac�s de lignes et de cercles
/**************************************************************************/
#include <stdlib.h>
#include <GL/glut.h>
#include <iostream>
#include <vector>
#include "BSplineCurve.h"

#define MAXSHAPES 256

//void createMenu();
void display();
void drawCircle(int x0, int y0, int x1, int y1);
void drawLine(int x1, int y1, int x2, int y2);
void drawShapes();
void menuFunction(int value);
//void mouseDrag(int x, int y);
void mouseInput(int button, int state, int x, int y);
void reshape(int width, int height);


typedef enum mouseState {
	waiting,
	clicked,
	lastVertex,
};

BSplineCurve* curve;
int counter = 0;

int mouseState = waiting; // �tat de la souris - en attente de clic

int vx1 = 0, vx2 = 400, vy1 = 0, vy2 = 400; // taille de la fen�tre

float RVB[3] = { 1.0, 1.0, 1.0 }; // stockage de la couleur de dessin
Color c;
/*
// fonctions du menu 
void menuFunction(int value) {
	switch (value) {
	case 0:
		c.r = 1.0;
		c.g = 0.0;
		c.b = 0.0;
		break;
	case 1:
		c.r = 0.0;
		c.g = 1.0;
		c.b = 0.0;
		break;
	case 2:
		c.r = 0.0;
		c.g = 0.0;
		c.b = 1.0;
		break;
	case 3:
		c.r = 1.0;
		c.g = 1.0;
		c.b = 0.0;
		break;
	case 4:
		c.r = 1.0;
		c.g = 0.0;
		c.b = 1.0;
		break;
	case 5:
		c.r = 0.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	case 6:
		c.r = 1.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	case 8:
		mouseState = lastVertex;
		break;
	case 9:
		delete curve;
		curve = nullptr;
		counter = 0;
		break;
	default: // Si on appuie sur la ligne s�paratrice : blanc
		c.r = 1.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	}
}

// initialisation du menu
void createMenu() {
	int menu = glutCreateMenu(menuFunction);

	//glutAddMenuEntry("Red", 0);
	glutAddMenuEntry("Green", 1);
	glutAddMenuEntry("Blue", 2);
	glutAddMenuEntry("Yellow", 3);
	glutAddMenuEntry("Magenta", 4);
	glutAddMenuEntry("Cyan", 5);
	glutAddMenuEntry("White", 6);
	glutAddMenuEntry("--------------", 7);
	glutAddMenuEntry("Line Mode", 8);
	glutAddMenuEntry("Start Clipping Polygon", 9);
	glutAddMenuEntry("End Clipping Polygon", 10);
	glutAddMenuEntry("Clear Clipping Polygon", 11);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}
*/

/*
//cette fonction est la fonction de gestion des mouvement de la souris
void mouseDrag(int x, int y) {
	//std::cout << x << "," << y << std::endl;
	display();
}
*/
//cette fonction g�re les click souris
void mouseInput(int button, int state, int x, int y) {
	//si on a appuy� sur le bouton gauche
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		switch (mouseState) {
			case waiting:
				delete curve;
				curve = new BSplineCurve();
				curve->addPoint(x, vy2 - y);
				counter++;
				mouseState = clicked;
				break;
			case clicked:
				curve->addPoint(x, vy2 - y);
				counter++;
				break;
			case lastVertex:
				curve->addPoint(x, vy2 - y);
				counter++;
				mouseState = waiting;
				break;
			default:
				break;
			}
		}
	display();
}

//Cette fonction est la fonction de gestion du redimensionnement
void reshape(int width, int height) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, width, 0, height);

	//mise � jour des valeurs de dimensions
	vx1 = 0;
	vx2 = width;
	vy1 = 0;
	vy2 = height;

	glMatrixMode(GL_MODELVIEW);

	//d�finition de la nouvelle vue
	//glViewport(0,0,largeur,hauteur);
}

void drawControlPoints() {
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < curve->controlPoints.size(); i++)
		glVertex2i(curve->controlPoints.at(i).x, curve->controlPoints.at(i).y);
	glEnd();
}

//cette fonction est la fonction d'affichage principale
void display() {
	glClear(GL_COLOR_BUFFER_BIT); // effacer l'�cran

	if (counter > 0) {
		drawControlPoints();
		curve->draw();
	}
	glFlush();
}

int main(int argc, char **argv) {
	glutInit(&argc, argv); //initialisation du toolkit glut

	// init window
	glutInitDisplayMode(GLUT_RGB); //initialisation du mode d'affichage
	glutInitWindowPosition(vx1, vy1); //initialisation de la position de la fen�tre
	glutInitWindowSize(vx2, vy2); //initialisation de la taille de la fen�tre
	glutCreateWindow("TP3 - Cyrus-Beck Clipping"); //cr�ation de la fen�tre

	// GLUT callbacks
	glutDisplayFunc(display); //attachement de la fonction d'affichage
	glutReshapeFunc(reshape); //attachement de la fonction de redimensionnement
	glutMouseFunc(mouseInput); //attachement de la fonction de gestion de la souris
	//glutPassiveMotionFunc(mouseDrag); //attachement de la fonction de gestion des mouvement de la souris

	// init OpenGL settings
	glClearColor(0.0, 0.0, 0.0, 0.0); //couleur d'effacement: noir
	glMatrixMode(GL_PROJECTION);
	glOrtho(-1, 1.0, -1, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glPointSize(1.0); //taille d'un point: 1 pixels
	glColor3f(0.0, 1.0, 0.0); //couleur verte

	//createMenu();

	//lancement de la boucle principale
	glutMainLoop();

	return 0;
}