#include "BSplineCurve.h"


BSplineCurve::BSplineCurve()
{
}


BSplineCurve::~BSplineCurve()
{
}


void BSplineCurve::draw() {
	glBegin(GL_POINTS);
	glColor3f(0, 1, 0);
	glPointSize(1.0);

	for (float t = 0; t <= 1; t += 0.005) { // pas de 0.05
		for (int i = 3; i < controlPoints.size(); i++) {
			float xPoint = 0;
			float yPoint = 0;

			float coef1 = (float)pow((1 - t), 3) / 6.f;
			float coef2 = pow(t, 3) / 2.f - pow(t, 2) + 2.f / 3.f;
			float coef3 = (1 / 6.f) * (-3 * pow(t, 3) + 3 * pow(t, 2) + 3 * t + 1);
			float coef4 = (pow(t, 3) / 6.f);

			xPoint = controlPoints.at(i - 3).x  * coef1;
			yPoint = controlPoints.at(i - 3).y * coef1;

			xPoint += controlPoints.at(i - 2).x * coef2;
			yPoint += controlPoints.at(i - 2).y * coef2;

			xPoint += controlPoints.at(i - 1).x * coef3;
			yPoint += controlPoints.at(i - 1).y * coef3;

			xPoint += controlPoints.at(i).x * coef4;
			yPoint += controlPoints.at(i).y * coef4;

			glVertex2f(xPoint, yPoint);
		}
	}

	glEnd();
}

void BSplineCurve::addPoint(GLfloat x, GLfloat y){
	Point2f p;
	p.x = x;
	p.y = y;
	controlPoints.push_back(p);
}
void BSplineCurve::clear() {
}