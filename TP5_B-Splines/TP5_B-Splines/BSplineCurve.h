#pragma once
#include <GL\glut.h>
#include <vector>

typedef struct Point2i {
	GLint x;
	GLint y;
} Point2i;

typedef struct Point2f {
	GLfloat x;
	GLfloat y;
} Point2f;

typedef struct Color {
	float r;
	float g;
	float b;
} Color;


class BSplineCurve {
public:
	BSplineCurve();
	std::vector<Point2f> controlPoints;
	~BSplineCurve();

	void draw();
	void addPoint(GLfloat x,GLfloat y);
	void clear();

};

