#include "Line.h"


Line::Line(float x1, float y1, float x2, float y2, float r, float g, float b) : Shape(r, g, b) {
	_start.x = x1;
	_start.y = y1;
	_end.x = x2;
	_end.y = y2;
}

Line::~Line() {}

void Line::draw() {
	glLineWidth(1.0);
	glColor3f(_color.r, _color.g, _color.b);
	glBegin(GL_LINES);
	glVertex2f(_start.x, _start.y);
	glVertex2f(_end.x, _end.y);
	glEnd();
}

Point2f Line::getDirectionVector() {
	Point2f p;
	p.x = _end.x - _start.x;
	p.y = _end.y - _start.y;
	return p;
}

Point2f Line::getStart() {
	return this->_start;
}

Point2f Line::getEnd() {
	return this->_end;
}

void Line::setStart(float x, float y) {
	this->_start.x = x;
	this->_start.y = y;
}

void Line::setEnd(float x, float y) {
	this->_end.x = x;
	this->_end.y = y;
}