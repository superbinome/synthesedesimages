#pragma once
#include "Shape.h"
#include <cmath>
#include <math.h>

class Circle : public Shape {
private:
	Point2f _centre;
	float _radius;
public:
	Circle(float x1, float y1, float x2, float y2, float r, float g, float b);
	~Circle();

	void draw();
	
	void setEnd(float x, float y);
};

