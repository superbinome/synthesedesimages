#include "Polygon.h"

Polygon::Polygon(float x, float y, float r, float g, float b) : Shape(r, g, b) {
	Point2f t_p;
	t_p.x = x;
	t_p.y = y;

	_points = new std::vector<Point2f>();
	_points->push_back(t_p); // point de d�but
	_points->push_back(t_p); // point de fin
}

Polygon::~Polygon() {

}

void Polygon::draw() {
	glColor3f(_color.r, _color.g, _color.b);
	glBegin(GL_LINE_LOOP);
	for (auto it = _points->begin(); it != _points->end(); it++)
		glVertex2i(it->x, it->y);
	glEnd();
}

void Polygon::setEnd(float x, float y) {
	_points->back().x = x;
	_points->back().y = y;
}

void Polygon::addPoint(float x, float y) {
	Point2f t_p;
	t_p.x = x;
	t_p.y = y;
	_points->push_back(t_p);
}

std::vector<Point2f>* Polygon::getPoints() {
	return _points;
}

std::vector<Point2f>* Polygon::computeNormals() {
	int i, j, k;
	Point2f v;
	std::vector<Point2f>* normals = new std::vector<Point2f>();
	// On parcourt les sommets du polygone
	for (i = 0; i < _points->size(); i++) {
		// ij : C�t� auquel on calcule la normale interne
		j = (i + 1) % _points->size();
		// jk = C�t� suivant
		k = (i + 2) % _points->size();
		Point2f tempPoint;
		tempPoint.x = -(_points->at(j).y - _points->at(i).y) / (_points->at(j).x - _points->at(i).x);
		tempPoint.y = 1.0;
		normals->push_back(tempPoint);
		v.x = _points->at(k).x - _points->at(i).x;
		v.y = _points->at(k).y - _points->at(i).y;

		float dotProductNIxV = normals->at(i).x * v.x + normals->at(i).y * v.y;
		if (dotProductNIxV > 0) {
			normals->at(i).x *= -1;
			normals->at(i).y = -1;
		}
	}
	return normals;
}