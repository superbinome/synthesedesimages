/**************************************************************************/
/* TP2 - Trac�s de lignes et de cercles
/**************************************************************************/
#include <stdlib.h>
#include <GL/glut.h>
#include <iostream>
#include <vector>
#include "Line.h"
#include "Polygon.h"

#define MAXSHAPES 256

void createMenu();
void display();
void drawCircle(int x0, int y0, int x1, int y1);
void drawLine(int x1, int y1, int x2, int y2);
void drawShapes();
void menuFunction(int value);
void mouseDrag(int x, int y);
void mouseInput(int button, int state, int x, int y);
void reshape(int width, int height);
void CyrusBeckClip();


typedef enum mouseState {
	waiting,
	clicked,
	lastVertex,
};

typedef enum mode {
	line,
	polygon,
};

std::vector<Line*>* lines;
Polygon* CBPoly;
int pcounter = 0;

int mouseState = waiting; // �tat de la souris - en attente de clic
int mode = polygon; //mode du trac�

int vx1=0,vx2=400,vy1=0,vy2=400; // taille de la fen�tre

float RVB[3] = { 1.0, 1.0, 1.0 }; // stockage de la couleur de dessin
Color c;

// fonctions du menu 
void menuFunction(int value) {
	switch (value) {
	case 0:
		c.r = 1.0;
		c.g = 0.0;
		c.b = 0.0;
		break;
	case 1:
		c.r = 0.0;
		c.g = 1.0;
		c.b = 0.0;
		break;
	case 2:
		c.r = 0.0;
		c.g = 0.0;
		c.b = 1.0;
		break;
	case 3:
		c.r = 1.0;
		c.g = 1.0;
		c.b = 0.0;
		break;
	case 4:
		c.r = 1.0;
		c.g = 0.0;
		c.b = 1.0;
		break;
	case 5:
		c.r = 0.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	case 6:
		c.r = 1.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	case 8:
		if (pcounter > 3) mode = line; // on passe en mode ligne seulement si le polygone a plus de 3 c�t�s
		break;
	case 9:
		mode = polygon; // on passe en mode polygone
		break;
	case 10: 
		mouseState = lastVertex;
		break;
	case 11:
		delete CBPoly;
		CBPoly = nullptr;
		pcounter = 0;
		break;
	default: // Si on appuie sur la ligne s�paratrice : blanc
		c.r = 1.0;
		c.g = 1.0;
		c.b = 1.0;
		break;
	}
	if (!lines->empty())
		lines->back()->setColor(c);
}

// initialisation du menu
void createMenu() {
	int menu = glutCreateMenu(menuFunction);

	//glutAddMenuEntry("Red", 0);
	glutAddMenuEntry("Green", 1);
	glutAddMenuEntry("Blue", 2);
	glutAddMenuEntry("Yellow", 3);
	glutAddMenuEntry("Magenta", 4);
	glutAddMenuEntry("Cyan", 5);
	glutAddMenuEntry("White",6);
	glutAddMenuEntry("--------------", 7);
	glutAddMenuEntry("Line Mode", 8);
	glutAddMenuEntry("Start Clipping Polygon", 9);
	glutAddMenuEntry("End Clipping Polygon", 10);
	glutAddMenuEntry("Clear Clipping Polygon", 11);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

//cette fonction est la fonction de gestion des mouvement de la souris
void mouseDrag(int x,int y) {
	if (mouseState != waiting) {
		if (mode == line)
			lines->back()->setEnd(x, vy2 - y);
		else
			CBPoly->setEnd(x, vy2 - y);
	}
	//std::cout << x << "," << y << std::endl;
	display();
}

//cette fonction g�re les click souris
void mouseInput(int button, int state, int x, int y) {
	//si on a appuy� sur le bouton gauche
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		if (mode == line) {
			switch (mouseState) {
			case waiting: //d�finition du premier point
				lines->push_back(new Line(x, vy2 - y, x, vy2 - y, c.r, c.g, c.b));
				mouseState = clicked;
				break;
			case clicked: //d�finition du second point
				lines->back()->setEnd(x, vy2 - y);
				mouseState = waiting;
				break;
			default:
				break;
			}
			//std::cout << "clic:" << x << "," << y << std::endl;
		} else {
			switch (mouseState) {
			case waiting:
				delete CBPoly;
				CBPoly = new Polygon(x, vy2 - y, c.r, c.g, c.b);
				pcounter++;
				mouseState = clicked;
				break;
			case clicked:
				CBPoly->addPoint(x, vy2 - y);
				pcounter++;
				break;
			case lastVertex:
				CBPoly->addPoint(x, vy2 - y);
				pcounter++;
				mouseState = waiting;
				mode = line;
				break;
			default:
				break;
			}
		}
	}
	display();
}

//Cette fonction est la fonction de gestion du redimensionnement
void reshape(int width,int height) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, width, 0, height);
	
	//mise � jour des valeurs de dimensions
	vx1=0;
	vx2=width;
	vy1=0;
	vy2=height;

	glMatrixMode(GL_MODELVIEW);

	//d�finition de la nouvelle vue
	//glViewport(0,0,largeur,hauteur);
}

void drawShapes() {
	for (auto it = lines->begin(); it != lines->end(); it++)
		(*it)->draw();
	if (CBPoly != nullptr) {
		CBPoly->draw();
	}
}

float dotProduct(Point2f p1, Point2f p2) {
	return p1.x*p2.x + p1.y*p2.y;
}

void CyrusBeckClip() {
	std::vector<Point2f>* Polygon = CBPoly->getPoints();
	std::vector<Point2f>* normals = CBPoly->computeNormals();
	for (std::vector<Line*>::iterator it = lines->begin(); it != lines->end(); it++) {
		float t_enter, t_leave, t, num, den;
		t_enter = 0.0;
		t_leave = 1.0;
		
		Point2f dirV = (*it)->getDirectionVector();
		Point2f F;
		Point2f start = (*it)->getStart();
		Point2f end = (*it)->getEnd();

		Point2f cstart, cend;

		bool visible = true;

		for (int i = 0; i < Polygon->size(); i++) {
			F.x = start.x - Polygon->at(i).x;
			F.y = start.y - Polygon->at(i).y;

			num = dotProduct(normals->at(i), F);
			den = dotProduct(normals->at(i), dirV);


			if (den == 0.0) { // Parallele ou point
				// parallele : si en dehors, on oublie la ligne, si dedans, pas d'intersections avec ce c�t� du poly
				if (num > 0.0)
					visible = false;
			}
			else {
				t = -(num / den);
				if (den < 0.0) { // entrant
					if (t <= 1.0)
						if (t > t_enter)
							t_enter = t;
				}
				else if (t >= 0.0) { // sortant
					if (t < t_leave)
						t_leave = t;
				}
			}
		}
		if (t_enter <= t_leave) {
			cstart.x = start.x + t_enter * dirV.x;
			cstart.y = start.y + t_enter * dirV.y;
			cend.x = start.x + t_leave * dirV.x;
			cend.y = start.y + t_leave * dirV.y;
		}
		else {
			visible = false;
		}
		
		if (visible) {
			glPointSize(2.0);
			glColor3f(1.0f, 0.0f, 0.0f);
			glBegin(GL_LINES);
			glVertex2f(cstart.x, cstart.y);
			glVertex2f(cend.x, cend.y);
			glEnd();
		}
	}
}

//cette fonction est la fonction d'affichage principale
void display() {
	glClear(GL_COLOR_BUFFER_BIT); // effacer l'�cran

	drawShapes();

	if (pcounter >= 3 && mode == line && lines->size() > 0)
		CyrusBeckClip();

	glFlush();
}

int main(int argc,char **argv) {
	glutInit(&argc,argv); //initialisation du toolkit glut

	// init window
	glutInitDisplayMode(GLUT_RGB); //initialisation du mode d'affichage
	glutInitWindowPosition(vx1,vy1); //initialisation de la position de la fen�tre
	glutInitWindowSize(vx2,vy2); //initialisation de la taille de la fen�tre
	glutCreateWindow("TP3 - Cyrus-Beck Clipping"); //cr�ation de la fen�tre

	// GLUT callbacks
	glutDisplayFunc(display); //attachement de la fonction d'affichage
	glutReshapeFunc(reshape); //attachement de la fonction de redimensionnement
	glutMouseFunc(mouseInput); //attachement de la fonction de gestion de la souris
	glutPassiveMotionFunc(mouseDrag); //attachement de la fonction de gestion des mouvement de la souris

	// init OpenGL settings
	glClearColor(0.0, 0.0, 0.0, 0.0); //couleur d'effacement: noir
	glMatrixMode(GL_PROJECTION);
	glOrtho(-1, 1.0, -1, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glPointSize(1.0); //taille d'un point: 1 pixels
	glColor3f(0.0, 1.0, 0.0); //couleur verte

	lines = new std::vector<Line*>();
	c.r = 0.0; c.g = 1.0; c.b = 0.0;

	createMenu();

	//lancement de la boucle principale
	glutMainLoop();

	return 0;
}