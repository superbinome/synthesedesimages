#include "Circle.h"


Circle::Circle(float x1, float y1, float x2, float y2, float r, float g, float b) : Shape(r, g, b) {
	_centre.x = x1;
	_centre.y = y1;
	_radius = sqrt(pow(x1 - y1, 2) + pow(x2 - y2, 2));
	//_end.x = x2;
	//_end.y = y2;
}

Circle::~Circle(){}

void Circle::draw() {
	float x1 = _centre.x;
	float y1 = _centre.y;
	//int x2 = _end.x;
	//int y2 = _end.y;

	glColor3f(_color.r, _color.g, _color.b);
	glBegin(GL_POINTS);

	//glPointSize(1.0);
	//glOrtho(vx1 - vx2 / 2, vx2 / 2, vy1 - vy2 / 2, vy2 / 2, -1.0, 1.0); //coordonnées des limites de la vue

	float x = _radius;
	float y = 0;
	float radiusError = 1 - x;

	while (x >= y) {
		glVertex2f(x + x1, y + y1);
		glVertex2f(y + x1, x + y1);
		glVertex2f(-x + x1, y + y1);
		glVertex2f(-y + x1, x + y1);
		glVertex2f(-x + x1, -y + y1);
		glVertex2f(-y + x1, -x + y1);
		glVertex2f(x + x1, -y + y1);
		glVertex2f(y + x1, -x + y1);
		y++;
		if (radiusError<0){
			radiusError += 2 * y + 1;
		}
		else {
			x--;
			radiusError += 2 * (y - x + 1);
		}
	}
	glEnd();
}

void Circle::setEnd(float x, float y) {
	_radius = sqrt(pow(_centre.x - _centre.y, 2) + pow(x - y, 2));
}