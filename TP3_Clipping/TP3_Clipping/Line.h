#pragma once
#include "Shape.h"
class Line : public Shape {
private:
	Point2f _start;
	Point2f _end;
public:
	Line(float x1, float y1, float x2, float y2, float r, float g, float b);
	Line(Point2f start, Point2f end, float r, float g, float b);
	~Line();

	void draw();
	Point2f getDirectionVector();

	Point2f getStart();
	Point2f getEnd();

	void setStart(float x, float y);
	void setEnd(float x, float y);
};

