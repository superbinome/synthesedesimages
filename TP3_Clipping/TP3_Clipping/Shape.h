#pragma once
#include <stdlib.h>
#include "glut.h"

typedef struct Point2i {
	int x;
	int y;
} Point2i;

typedef struct Point2f {
	float x;
	float y;
} Point2f;

typedef struct Color {
	float r;
	float g;
	float b;
} Color;

class Shape {
protected:
	Color _color;
public:
	Shape(float r, float g, float b);
	virtual ~Shape();

	virtual void draw() =0;

	Color getColor();

	virtual void setEnd(float x, float y) = 0;
	void setColor(Color color);
};