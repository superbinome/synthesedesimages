#pragma once
#include "Shape.h"
#include <vector>

class Polygon : public Shape {
private:
	std::vector<Point2f>* _points;
	float _radius;
public:
	Polygon(float x, float y, float r, float g, float b);
	~Polygon();

	void draw();

	std::vector<Point2f>* computeNormals();

	std::vector<Point2f>* getPoints();
	void addPoint(float x, float y);
	void setEnd(float x, float y);
};