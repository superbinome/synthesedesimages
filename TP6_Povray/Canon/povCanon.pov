#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"
#include "skies.inc"
   




   
   
// sun ---------------------------------------------------------------------
light_source{<1500,2500,-2500> color White}


// ground ------------------------------------------------------------------
plane { <0,1,0>, 0 
        texture{ pigment{ color rgb<0.35,0.65,0.0>*0.9 }
	         normal { bumps 0.75 scale 0.015 }
                 finish { phong 0.1 }
               } // end of texture
      } // end of plane



//--------------------------------------------------------------------------
//---------------------------- objects in scene ----------------------------
//-------------------------------------------------------------------------- 
    
    
#declare support_canon = box
{     
   // dimensions of the box
   <-1, -0.1, -2 ><1, 0.1, 2>
   
   // texture 
   texture
   {
       T_Wood24    
       normal { wood 0.5 scale 0.05 turbulence 0.1 rotate<0,0,0> }
       finish { phong 1 } 
       rotate<0,0,0> scale 0.5 translate<0,0,0>
   } // end of texture 

}


// The cylinders used to make the canon
#declare cyl = cylinder
{
    <0, 0, -2><0, 0, 2>1
    pigment {color red 0.19 green 0.19 blue 0.19}
    finish {phong .2 reflection {.3}}
}   

#declare cyl2 = cylinder
{
    <0, 0, -2><0, 0, 1>1.05
    pigment {color red 0.19 green 0.19 blue 0.19}
    finish {phong .2 reflection {.3}}
}    

#declare cyl3 = cylinder
{
    <0, 0, -0.02><0, 0, 0.02>1.1
    pigment {color red 0.19 green 0.19 blue 0.19}
    finish {phong .2 reflection {.3}}
}

camera
{   
    
    location <10, 6, 0>
    look_at  <0, 0, 0>    // look the center
}     

#declare boulet = object
{
    sphere { <0 0 0> 0.8 }
    pigment {color red 0.19 green 0.19 blue 0.19}
    finish {phong .2 reflection {.3}}  
 
      texture
      {
            T_Stone10    
                normal { agate 0.25 scale 0.15 rotate<0,0,0> }
                finish { phong 1 } 
                rotate<0,0,0> scale 0.5 translate<0,0,0>
      } // end of texture 
}

#declare demi_sph = object {
    intersection {
        sphere { <0 0 0> 1 }
        plane { <0 0 1> 0 }     
pigment {color red 0.19 green 0.19 blue 0.19}
finish {phong .2 reflection {.3}}   

}
}    

#declare roue =                                     
cylinder
{                                                                                                                                          
    < -0.15, 0, 0><0.15, 0, 0>0.6
    texture {T_Wood2}
    finish {phong .2 reflection {.3}}                                   
}  


#declare roue2 =                                     
cylinder
{
    < -0.15, 0, 0><0.15, 0, 0>0.8
    texture {T_Wood2}
    finish {phong .2 reflection {.3}}  
}


#declare canon =
object
{
    union                                                                                                                 
    {    
        object{cyl}  
        object{cyl2 translate <-0, -0, 2>}    
        object{cyl3 translate <-0, -0, 3>}                                                                                                                                  
        object{demi_sph  translate <-0, -0, -2>} 
    }    
}          


#declare support_total_canon = object
{
    union {
        object{roue translate <1, -0.25, 0>}     
        object{roue translate <-1, -0.25, 0>}     
        object{roue2 translate <1, 0, 2>}     
        object{roue2 translate <-1, 0, 2>}
        object{support_canon translate <0, 0, 1>}
    }
}



 
object{canon translate<0, 2, 1.5> rotate<-20,0, 0>}     
object{support_total_canon  translate<0, 0.8, 0> scale <1.3, 1.3, 1.3>   } 

object{boulet translate<2,0.8,-2> }
object{boulet translate<1.7,0.8,-4>}
object{boulet translate<0.5,0.8,-2.8>}
       


                                                      
                               